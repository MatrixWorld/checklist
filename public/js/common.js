$(document).ready(function() {


	$("#add").on('click', function() {
		var id = $('.custom-control-input:last').attr('name');
		console.log(id);
		var count_id = $('.custom-control-input');
		var len_id = count_id.length;
		console.log(len_id);
		$(`
			<div class="form-group">
				<div class="input-group">
					<div class="custom-control custom-checkbox float-sm-left">
						<input class="custom-control-input" id="check_box[`+ len_id +`]" name="check_box[]" type="checkbox">
						<input type="hidden" name="id[]" value="">
						<label class="custom-control-label" for="check_box[`+ len_id +`]"></label>
					</div>

					<input class="form-control" value="" type="text" name="task[]" id="task[`+ len_id +`]" required>
					<label class="control-label" for="task[`+ len_id +`]"></label>
				</div>
			</div>
			<hr>
		`).insertAfter('hr:last');
		if (len_id == 10) {
			$('#add').attr('disabled', true);
		}
    });
});
