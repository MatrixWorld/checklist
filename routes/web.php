<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => [ 'auth', 'un_auth:No' ]], function() {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('admin', 'Checklist\AdminController@index')->name('admin');
    Route::get('admin/{user}/user_checklist', 'Checklist\AdminController@user_checklist')->name('user_checklist');
    Route::get('admin/{user}/{checklist}/user_tasks', 'Checklist\AdminController@user_tasks')->name('user_tasks');
    Route::put('admin/{user}/allow_checklist', 'Checklist\AdminController@allow_checklist')->name('allow_checklist');
    Route::put('admin/{user}/new_role', 'Checklist\AdminController@new_role')->name('new_role');
    Route::put('admin/{user}/blocked', 'Checklist\AdminController@blocked')->name('blocked');

    Route::resource('checklists', 'Checklist\ChecklistController');
    Route::get('checklists/create', 'Checklist\ChecklistController@create')->name('create');
    Route::get('checklists/{checklist}/destroy', 'Checklist\ChecklistController@destroy');
    Route::delete('checklists/{checklist}/destroy', 'Checklist\ChecklistController@destroy')->name('destroy');
    Route::post('checklists/{checklist}/edit','Checklist\ChecklistController@update');
    Route::put('checklists/{checklist}/edit','Checklist\ChecklistController@update');

    Route::resource('tasks', 'Checklist\Tasks_checklistController', ['only' => 'store', 'update', 'destroy']);
    Route::get('tasks', 'Checklist\Tasks_checklistController@store');
    Route::get('tasks/{task}','Checklist\Tasks_checklistController@update');
    Route::put('tasks/{task}','Checklist\Tasks_checklistController@update');
    Route::get('tasks/{task}/destroy', 'Checklist\Tasks_checklistController@destroy')->name('del_task');
});
