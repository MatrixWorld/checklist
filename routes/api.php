<?php

use Illuminate\Http\Request;
use App\Http\Resources\UserResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return new UserResource($request->user());
});

Route::group(['namespace' => 'Api'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::post('register', 'RegisterController');
        Route::post('login', 'LoginController');
        Route::post('logout', 'LogoutController')->middleware('auth:api');
    });

    Route::group(['namespace' => 'Checklist', 'middleware' => 'auth:api'], function () {
        Route::apiResource('/checklists', 'ChecklistController');

        Route::get('checklists/{checklist}/tasks', 'Tasks_checklistController@index');
        Route::get('checklists/{checklist}/tasks/{task}', 'Tasks_checklistController@show');
        Route::post('checklists/{checklist}/tasks', 'Tasks_checklistController@store');
        Route::put('checklists/{checklist}/tasks/{task}', 'Tasks_checklistController@update');
        Route::delete('checklists/{checklist}/tasks/{task}/delete', 'Tasks_checklistController@destroy');
        Route::delete('checklists/{checklist}/tasks/delete', 'Tasks_checklistController@destroy');
    });
});
