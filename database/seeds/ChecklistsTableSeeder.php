<?php

use Illuminate\Database\Seeder;

class ChecklistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1; $i<11; $i++) {
            factory(App\Models\Checklist::class, 6)->create(
                [
                    'user_id' => $i,
                ]
            );
            // ->each(function ($u) {
            //     $u->tasks()->save(factory(App\Models\Tasks_checklist::class)->make());
            // });
        }
    }
}
