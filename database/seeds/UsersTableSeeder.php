<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 1)->create(
            [
                'password' => bcrypt('password'),
                'remember_token' => Str::random(10),
                'role' => 'Admin',
                'allow_checklist' => 10,
            ]
        );
        factory(App\Models\User::class, 3)->create(
            [
                'password' => bcrypt('password'),
                'remember_token' => Str::random(10),
                'role' => 'Moderator',
                'allow_checklist' => 8,
            ]
        );
        factory(App\Models\User::class, 1)->create(
            [
                'password' => bcrypt('password'),
                'remember_token' => Str::random(10),
                'role' => 'Moderator',
                'allow_checklist' => 8,
                'blocked' => 'Yes',
            ]
        );
        factory(App\Models\User::class, 1)->create(
            [
                'password' => bcrypt('password'),
                'remember_token' => Str::random(10),
                'role' => 'User',
                'allow_checklist' => 6,
                'blocked' => 'Yes',
            ]
        );
        factory(App\Models\User::class, 4)->create(
            [
                'password' => bcrypt('password'),
                'remember_token' => Str::random(10),
                'role' => 'User',
                'allow_checklist' => 6,
            ]
        );
    }
}
