<?php

use Illuminate\Database\Seeder;

class Tasks_checklistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1; $i<61; $i++) {
            factory(App\Models\Tasks_checklist::class, 7)->create(
                [
                    'checklist_id' => $i,
                ]
            );
        }
    }
}
