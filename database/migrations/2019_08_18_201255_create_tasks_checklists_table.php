<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks_checklists', function (Blueprint $table) {
            $table->increments('id');
            $table->text('task');
            $table->boolean('check_box')->default(false);
            $table->integer('checklist_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('tasks_checklists', function (Blueprint $table) {
            $table->foreign('checklist_id')->references('id')->on('checklists')
                        ->onDelete('restrict')
                        ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks_checklists', function (Blueprint $table) {
            $table->dropForeign('tasks_checklists_checklist_id_foreign');
        });

        Schema::dropIfExists('tasks_checklists');
    }
}
