<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Tasks_checklist;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Tasks_checklist::class, function (Faker $faker) {
    return [
        'task' => 'Задача ' . str_random(15),
        'check_box' => false,
    ];
});
