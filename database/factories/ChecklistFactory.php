<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Checklist;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

use App\Models\User;

$factory->define(Checklist::class, function (Faker $faker) {
    return [
        'checklist_name' => 'Лист ' . str_random(15),
        'description' => 'Описание ' . str_random(20),
    ];
});
