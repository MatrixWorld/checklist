@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header font-weight-bold">Ваши чек-листы, {{ Auth::user()->name }}</div>
                @can ('except_user', Auth::user())
                    <div class="card-header font-weight-bold">
                        <a href="{{ route('admin') }}">Админка</a>
                    </div>
                @endcan

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ $ch_lists->links() }}

                    @if ($ch_lists->isEmpty())
                        <a href="{{ route('create') }}"><button id="add" class="btn btn-light border-primary" type="button">Создать</button></a>
                        <br>
                    @else
                        @foreach ($ch_lists as $list)
                            <div class="font-weight-bold">
                                <a href="{{ url('/checklists/'.$list->id) }}">{{ $list['checklist_name'] }}</a>
                            </div>
                            <div class="">
                                {{ $list['description'] }}
                            </div>
                            <hr>
                        @endforeach
                        @if ( $ch_lists->total() >= $list->users['allow_checklist'] )
                            <a href="#"><button id="add" class="btn btn-light border-primary" type="button" disabled>Создать</button></a>
                            <br>
                        @else
                            <a href="{{ route('create') }}"><button id="add" class="btn btn-light border-primary" type="button">Создать</button></a>
                            <br>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
