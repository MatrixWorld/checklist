@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb font-weight-bold">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Главная</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Админка</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Чек-листы пользователя {{ $user->name }}</li>
                    </ol>
                </nav>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($checklists->isEmpty())
                        <div class="alert alert-success" role="alert">
                            {{ session('status', 'Пользователь ' . $user->name . ' еще не создал ничего') }}
                        </div>
                        <br>
                    @else
                        {{$checklists->links()}}
                        @foreach ($checklists as $list)
                            <div class="font-weight-bold">
                                <a href="{{ route('user_tasks', ['user_id'=>$user->id, 'checklist_id'=>$list->id]) }}">{{ $list['checklist_name'] }}</a>
                            </div>
                            <div class="">
                                {{ $list['description'] }}
                            </div>
                            <hr>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
