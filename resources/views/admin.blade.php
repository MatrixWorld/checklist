@include('layouts.header')

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Checklist') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="card">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb font-weight-bold">
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Главная</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Админка</li>
                                </ol>
                            </nav>

                            <div class="card-body">

                                {{ $users->links() }}

                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <table class="table">
                                    <thead class="thead-light">
                                        <tr class="text-sm-center">
                                            <th>№</th>
                                            <th>Имя</th>
                                            <th>Роль</th>
                                            <th>Всего ч/л</th>
                                            <th>МАХ ч/л</th>
                                            <th>Блокировка</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $user)
                                            <tr class="text-sm-center">
                                                <th scope="row">{{ $loop->iteration }}</th>
                                                <td>{{ $user['name'] }}</td>
                                                <td>
                                                    <form  method="POST" action="{{ route('new_role', ['user_id'=>$user['id']]) }}" name="form_new_role">
                                                        {{ method_field('PUT') }}
                                                        @csrf
                                                        @can ('only_admin', $user)
                                                            <select id="role" name="role" class="form-control">
                                                        @else
                                                            <select id="role" name="role" class="form-control" disabled>
                                                        @endcan
                                                                <option {{ $user['role'] == 'Admin' ? 'selected' : '' }}>Admin</option>
                                                                <option {{ $user['role'] == 'Moderator' ? 'selected' : '' }}>Moderator</option>
                                                                <option {{ $user['role'] == 'User' ? 'selected' : '' }}>User</option>
                                                            </select>
                                                            @can ('only_admin', $user)
                                                                <a href="#"><button class="btn btn-light border-primary ml-sm-1" type="submit" name="button_new_role">Применить</button></a>
                                                            @else
                                                                <a href="#"><button class="btn btn-light border-primary ml-sm-1" type="submit" name="button_new_role" disabled>Применить</button></a>
                                                            @endcan
                                                    </form>
                                                </td>
                                                <td><a href="{{ route('user_checklist', ['id'=>$user['id']]) }}">{{ $user->checklists()->count() }}</a></td>
                                                <td>
                                                    <form method="POST" action="{{ route('allow_checklist', ['user_id'=>$user['id']]) }}" name="form_allow_checklist">
                                                        {{ method_field('PUT') }}
                                                        @csrf
                                                        <div class="input-group">
                                                            @can ('only_admin', $user)
                                                                <input class="form-control float-sm-left" name="allow_checklist" type="text" value="{{ $user['allow_checklist'] }}">
                                                                <label class="control-label" for="allow_checklist"></label>
                                                                <a href="#"><button class="btn btn-light border-primary ml-sm-1" type="submit" name="button_allow_checklist">Изменить</button></a>
                                                            @else
                                                                <input class="form-control float-sm-left" name="allow_checklist" type="text" value="{{ $user['allow_checklist'] }}" readonly>
                                                                <label class="control-label" for="allow_checklist"></label>
                                                                <a href="#"><button class="btn btn-light border-primary ml-sm-1" type="submit" name="button_allow_checklist" disabled>Изменить</button></a>
                                                            @endcan
                                                        </div>
                                                    </form>
                                                </td>
                                                <td>
                                                    <form  method="POST" action="{{ route('blocked', ['user_id'=>$user['id']]) }}" name="form_blocked">
                                                        {{ method_field('PUT') }}
                                                        @csrf
                                                        @can ('except_user', $user)
                                                            <select id="blocked" name="blocked" class="form-control">
                                                        @else
                                                            <select id="blocked" name="blocked" class="form-control" disabled>
                                                        @endcan
                                                                <option {{ $user['blocked'] == 'No' ? 'selected' : '' }} value="No">Нет</option>
                                                                <option {{ $user['blocked'] == 'Yes' ? 'selected' : '' }} value="Yes">Да</option>
                                                            </select>

                                                        @can ('except_user', $user)
                                                            <a href="#"><button class="btn btn-light border-primary ml-sm-1" type="submit" name="button_blocked">Применить</button></a>
                                                        @else
                                                            <a href="#"><button class="btn btn-light border-primary ml-sm-1" type="submit" name="button_blocked" disabled>Применить</button></a>
                                                        @endcan
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
</html>
