@include('layouts.header')

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Checklist') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb font-weight-bold">
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Главная</a></li>
                                    <li class="breadcrumb-item"><a href="{{ url('/checklists/'.$checklist['id']) }}">{{ $checklist['checklist_name'] }}</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Редактирование</li>
                                </ol>
                            </nav>

                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <form id="form_checklist" method="POST" action="{{ action('Checklist\ChecklistController@update', ['checklist'=>$checklist['id']]) }}"/>

                                    <div class="form-group {{ $errors->has('checklist_name') ? ' has-error' : '' }}">
                                        <label for="checklist_name" class="col-md-4 control-label">Название</label>

                                        <div class="mb-4 font-weight-bold">
                                            <input id="checklist_name" type="text" class="form-control" name="checklist_name" value="{{ $checklist['checklist_name'] }}" required autofocus>

                                            @if ($errors->has('checklist_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('checklist_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label for="description" class="col-md-4 control-label">Описание</label>

                                        <div class="mb-4 font-weight-bold">
                                            <textarea id="description" type="text" class="form-control" name="description" required>{{ $checklist['description'] }}</textarea>

                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <hr>
                                    @foreach ($checklist['tasks'] as $task)
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="custom-control custom-checkbox float-sm-left">
                                                    <input class="custom-control-input" id="check_box[ {{ $task['id'] }} ]" name="check_box[]" type="checkbox" {{ $task['check_box'] == "1" ? 'checked' : ''}} value="1">
                                                    <input type="hidden" name="id[]" value="{{ $task['id'] }}">
                                                    <label class="custom-control-label" for="check_box[ {{ $task['id'] }} ]"></label>
                                                </div>

                                                <input class="form-control {{ $errors->has('task.*') ? ' has-error' : '' }}" value="{{ $task['task'] }}" type="text" name="task[]" id="task[ {{ $task['id'] }} ]" required>
                                                <label class="control-label" for="task[ {{ $task['id'] }} ]"></label>


                                                <a href="{{ route('del_task', [ 'task'=>$task['id'], 'del'=>'true' ]) }}"><button class="btn btn-light border-primary ml-sm-1" id="del_task[]" type="button">Удалить</button></a>
                                                <br>

                                            </div>
                                        </div>
                                        @if ($errors->has('task.*'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('task.*') }}</strong>
                                            </span>
                                        @endif
                                        <hr>
                                    @endforeach

                                    <button class="btn btn-light border-primary" id="btn_checklist" type="submit">Сохранить</button>
                                    <a href="#"><button id="add" class="btn btn-light border-primary" type="button">Добавить поле</button></a>
                                    <br>

                                    {{ method_field('PUT') }}
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>
</body>
</html>
