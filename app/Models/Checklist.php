<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Checklist
 *
 * @property int $id
 * @property string $checklist_name
 * @property string $description
 * @property int $user_id
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tasks_checklist[] $tasks
 * @property-read \App\Models\User $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checklist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checklist newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checklist query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checklist whereChecklistName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checklist whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checklist whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checklist whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checklist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checklist whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Checklist whereUserId($value)
 * @mixin \Eloquent
 */
class Checklist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'checklist_name', 'description', 'user_id'
    ];

    public function tasks()
    {
        return $this->hasMany('App\Models\Tasks_checklist', 'checklist_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
