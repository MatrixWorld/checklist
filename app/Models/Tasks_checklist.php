<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Tasks_checklist
 *
 * @property int $id
 * @property string $task
 * @property int $check_box
 * @property int $checklist_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $users
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tasks_checklist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tasks_checklist newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tasks_checklist query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tasks_checklist whereCheckBox($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tasks_checklist whereChecklistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tasks_checklist whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tasks_checklist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tasks_checklist whereTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tasks_checklist whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Tasks_checklist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task', 'check_box', 'checklist_id'
    ];

    public function users()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
