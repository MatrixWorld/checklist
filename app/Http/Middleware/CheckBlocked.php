<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $blocked)
    {
        if ( !$request->user()->hasBlocked($blocked) ) {
            Auth::logout($request);
            return redirect('/')->with('message', 'Аккаунт заблокирован. Обратитесь в службу поддержки');
        }
        return $next($request);
    }
}
