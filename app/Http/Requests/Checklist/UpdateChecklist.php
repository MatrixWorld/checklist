<?php

namespace App\Http\Requests\Checklist;

use App\Http\Requests\Checklist\StoreChecklist;

class UpdateChecklist extends StoreChecklist
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(),[
            //
        ]);
    }
}
