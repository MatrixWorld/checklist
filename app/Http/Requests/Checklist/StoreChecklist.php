<?php

namespace App\Http\Requests\Checklist;

use Illuminate\Foundation\Http\FormRequest;

class StoreChecklist extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'checklist_name' => 'required|min:15|max:200',
            'description' => 'required',
            'task.*' => 'required|min:15|max:200',
        ];
    }

    public function messages()
    {
        return [
            'checklist_name.required' => 'Поле :attribute обязательно для заполнения',
            'checklist_name.min' => 'Поле :attribute минимум 15 символов',
            'checklist_name.max' => 'Поле :attribute максимум 200 символов',
            'description.required' => 'Поле :attributes обязательно для заполнения',
            'task.*.required' => 'Поле :attribute обязательно для заполнения',
            'task.*.min' => 'Поле :attribute минимум 15 символов',
            'task.*.max' => 'Поле :attribute максимум 200 символов',
        ];
    }

    public function attributes()
    {
        return [
            'checklist_name' => 'НАЗВАНИЕ',
            'description' => 'ОПИСАНИЕ',
            'task.*' => 'ЗАДАЧА',
        ];
    }
}
