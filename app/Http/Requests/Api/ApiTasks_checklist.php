<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class ApiTasks_checklist extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'task' => 'required|min:15|max:200',
            'checklist_id' => 'required|integer|exists:checklists,id',
            'check_box' => 'boolean',
        ];

        switch ( $this->getMethod() )
        {
            case 'GET':
                return [
                    'checked' => 'string|min:4|max:5',
                ];
            case 'POST':
                return $rules;
            case 'PUT':
                return $rules;
            // case 'PATCH':
                // return $rules;
            // case 'DELETE':
                //return $rules;
        }
    }
}
