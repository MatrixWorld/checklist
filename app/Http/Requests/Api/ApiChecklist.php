<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class ApiChecklist extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'checklist_name' => 'required|min:15|max:200',
            'description' => 'required',
            'user_id' => 'required|integer|exists:users,id',
        ];

        switch ( $this->getMethod() )
        {
            case 'POST':
                return $rules;
            case 'PUT':
                return $rules;
            // case 'PATCH':
                //return $rules;
            // case 'DELETE':
                //return $rules;
        }
    }
}
