<?php

namespace App\Http\Controllers\Checklist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Checklist;
use App\Models\Tasks_checklist;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
   public function index()
   {
       $this->authorize('except_user', User::class);
       $users = User::paginate(5);
       return view('admin', ['users'=>$users]);
   }

   public function user_checklist($id)
   {
       $this->authorize('except_user', User::class);
       $user = User::findOrFail($id);
       $checklists = $user->checklists()->paginate(5);
       return view('user_checklist', ['user'=>$user, 'checklists'=>$checklists]);
   }

   public function user_tasks($user_id, $checklist_id)
   {
       $this->authorize('except_user', User::class);
       $user = User::findOrFail($user_id);
       $checklist = Checklist::findOrFail($checklist_id);
       return view('user_tasks', ['user'=>$user, 'checklist'=>$checklist]);
   }

   public function allow_checklist(Request $request, $user_id)
   {
       $this->authorize('only_admin', User::class);
       $user = User::findOrFail($user_id);
       $data = $request->all();
       $user_alow = $user->update($data);
       return redirect()->back()->with('status', 'Показатель обновлен');
   }

    public function new_role(Request $request, $user_id)
    {
        $this->authorize('only_admin', User::class);
        $user = User::findOrFail($user_id);
        $data = $request->all();
        $new_role = $user->update($data);
        return redirect()->back()->with('status', 'Роль обновлена');
    }

    public function blocked(Request $request, $user_id)
    {
        $this->authorize('except_user', User::class);
        $user = User::findOrFail($user_id);
        $data = $request->all();

        if ($user['role'] == 'Admin') {
            return redirect()->back()->with('status', 'Нельзя заблокировать администратора');
        } else {
            $block_user = $user->update($data);
        }

        if ($data['blocked'] == 'Yes') {
            return redirect()->back()->with('status', 'Пользователь заблокирован');
        }

        if ($data['blocked'] == 'No') {
            return redirect()->back()->with('status', 'Пользователь разблокирован');
        }
    }
}
