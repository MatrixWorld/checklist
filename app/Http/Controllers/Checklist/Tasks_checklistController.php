<?php

namespace App\Http\Controllers\Checklist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Checklist;
use App\Models\Tasks_checklist;
use Illuminate\Support\Facades\Route;

class Tasks_checklistController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('checklist');
        $checklist = Checklist::find($id);
        $form = $request->session()->pull('form');

        for ($i=0; $i < count($form['task']); $i++) {
            $save_form = [];

            if ( !isset($form['check_box'][$i]) ) {
                $form['check_box'][$i] = "0";
            }

            if ( !isset($form['id'][$i]) ) {
                if ( !isset($form['check_box']) ) {
                    $save_form['check_box'] = 0;
                } else {
                    $save_form['check_box'] = $form['check_box'][$i];
                }
                $save_form['task'] = $form['task'][$i];
                $save_form['_token'] = $form['_token'];
                $save_form['checklist_id'] = $checklist['id'];

                $new_task = Tasks_checklist::create($save_form);
            }
        }
        return redirect('checklists/' . $checklist['id']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = $request->session()->pull('form');
        $checklist = Checklist::find($id);

        for ($i=0; $i < count($form['task']); $i++) {
            $update_form = [];

            if ( !isset($form['check_box'][$i]) ) {
                $form['check_box'][$i] = 0;
            }

            if ( isset($form['id'][$i]) ) {
                $task = Tasks_checklist::findOrFail($form['id'][$i]);

                if ($form['id'][$i] == $task['id']) {
                    $update_form['check_box'] = $form['check_box'][$i];
                    $update_form['task'] = $form['task'][$i];
                    $update_form['_method'] = $form['_method'];
                    $update_form['_token'] = $form['_token'];

                    $task->update($update_form);
                }
            }

            if ( !isset($form['id'][$i]) ) {
                $request->session()->flash('form', $form);
                return redirect()->action('Checklist\Tasks_checklistController@store', [ 'checklist'=>$checklist['id'] ]);
            }
        }
        return redirect('checklists/' . $checklist['id']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $route = Route::currentRouteName();

        if ( $route == 'del_task' && isset($_GET['del']) && $_GET['del'] == 'true') {
            $task = Tasks_checklist::find($id);
            $task->delete();
            return redirect()->back()->with('status', 'Задача ' . $task['task'] . ' удалена');
        }

        if ( $route == 'del_task' && !isset($_GET['del']) ) {
            $checklist = Checklist::find($id);
            $tasks = $checklist->tasks;
            foreach ($tasks as $task) {
                $task->delete();
            }
            return redirect( route('destroy', [ 'checklist' => $checklist['id'] ]) );
        }
    }
}
