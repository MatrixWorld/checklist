<?php

namespace App\Http\Controllers\Checklist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Checklist;
use App\Models\Tasks_checklist;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Checklist\StoreChecklist;
use App\Http\Requests\Checklist\UpdateChecklist;

class ChecklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_allow_check = Auth::user()->allow_checklist;
        $checklists = Checklist::where('user_id', '=', Auth::user()->id)->get();
        if ( $checklists->count() >= $user_allow_check ) {
            return redirect('home')->with('status', 'Достигнуто максимальное количество чек-листов');
        } else {
            return view('checklistCreate');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChecklist $request)
    {
        $form = $request->all();
        $request->session()->flash('form', $form);
        $checklist = Checklist::create($form);
        return redirect()->action('Checklist\Tasks_checklistController@store', [ 'checklist'=>$checklist['id'] ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $checklist = Checklist::findOrFail($id);
        return view('checklists', ['checklist' => $checklist]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $checklist = Checklist::find($id);
        return view('checklistEdit', ['checklist'=>$checklist]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateChecklist $request, $id)
    {
        $checklist = Checklist::find($id);

        $form = $request->all();
        $request->session()->flash('form', $form);
        $checklist->update($form);
        return redirect()->action('Checklist\Tasks_checklistController@update', [ 'checklist'=>$checklist['id'] ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checklist = Checklist::find($id);
        $tasks = $checklist->tasks;
        if ($tasks->isEmpty()) {
            $checklist->delete();
        } else {
            return redirect( route('del_task', [ 'checklist' => $checklist['id'] ]) );
        }
        return redirect('home')->with('status', 'Чек-лист ' . $checklist['checklist_name'] . ' удален');
    }
}
