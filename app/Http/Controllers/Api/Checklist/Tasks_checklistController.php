<?php

namespace App\Http\Controllers\Api\Checklist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Checklist;
use App\Models\Tasks_checklist;
use Illuminate\Support\Facades\Auth;

use App\Http\Resources\Tasks_checklistResource;
use App\Http\Resources\Tasks_checklistCollection;
use App\Http\Requests\Api\ApiTasks_checklist;

class Tasks_checklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApiTasks_checklist $request, Checklist $checklist)
    {
        if ($checklist['user_id'] == Auth::guard('api')->user()->id) {
            $data = $request->all();
            $tasks = $checklist->tasks;

            if ( empty($data) ) {
                // return response()->json($tasks, 200);
                return new Tasks_checklistCollection($tasks);
            }

            if (isset($data['checked'])) {
                if ($data['checked'] == 'true') {
                    return new Tasks_checklistCollection($tasks->where('check_box', '=', 1));
                }
                if ($data['checked'] == 'false') {
                    return new Tasks_checklistCollection($tasks->where('check_box', '=', 0));
                }

                if ($data['checked'] != 'true' || $data['checked'] != 'false') {
                    return response()->json([
                        'message' => 'Вы не можете совершить эти действия',
                        'errors' => 'Доступ запрещен'
                    ], 403);
                }
            }

            if ( !empty($data) && !isset($data['checked']) ) {
                return response()->json([
                    'message' => 'Вы не можете совершить эти действия',
                    'errors' => 'Доступ запрещен'
                ], 403);
            }
        } else {
            return response()->json([
                'message' => 'Вы не можете совершить эти действия',
                'errors' => 'Доступ запрещен'
            ], 403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApiTasks_checklist $request, Checklist $checklist)
    {
        $user = Auth::guard('api')->user();
        $tasks = $checklist->tasks;
        $data = $request->all();

        if ($checklist['user_id'] == $user['id'] && $tasks->count() < 10) {
            $task = Tasks_checklist::create($data);
            // return response()->json($checklist, 201);
            return new Tasks_checklistResource($checklist);
        } else {
            return response()->json([
                'message' => 'Вы не можете совершить эти действия',
                'errors' => 'Доступ запрещен'
            ], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Checklist $checklist, Tasks_checklist $task)
    {
        if ($checklist['user_id'] == Auth::guard('api')->user()->id) {
            // return response()->json($checklist, 200);
            return new Tasks_checklistResource($task);
        } else {
            return response()->json([
                'message' => 'Вы не можете совершить эти действия',
                'errors' => 'Доступ запрещен'
            ], 403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ApiTasks_checklist $request, Checklist $checklist, Tasks_checklist $task)
    {
        $data = $request->all();

        if ($checklist['user_id'] == Auth::guard('api')->user()->id) {
            $task->update($request->all());
            return new Tasks_checklistResource($task);
        } else {
            return response()->json([
                'message' => 'Вы не можете совершить эти действия',
                'errors' => 'Доступ запрещен'
            ], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Checklist $checklist, Tasks_checklist $task)
    {
        if (empty($task['id'])) {
            if ($checklist['user_id'] == Auth::guard('api')->user()->id) {
                $tasks = $checklist->tasks;
                foreach ($tasks as $task) {
                    $task->delete();
                }
                return response()->json(null, 204);
            } else {
               return response()->json([
                   'message' => 'Вы не можете совершить эти действия',
                   'errors' => 'Доступ запрещен'
               ], 403);
           }
        }

        if (!empty($task['id'])) {
            if ($checklist['user_id'] == Auth::guard('api')->user()->id) {
                $task->delete();
                return response()->json(null, 204);
            } else {
                return response()->json([
                'message' => 'Вы не можете совершить эти действия',
                'errors' => 'Доступ запрещен'
                ], 403);
            }
        }
    }
}
