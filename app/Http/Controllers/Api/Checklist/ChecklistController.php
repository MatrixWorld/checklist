<?php

namespace App\Http\Controllers\Api\Checklist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Checklist;
use App\Models\Tasks_checklist;
use Illuminate\Support\Facades\Auth;

use App\Http\Resources\ChecklistResource;
use App\Http\Resources\ChecklistCollection;
use App\Http\Requests\Api\ApiChecklist;

class ChecklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $checklists = Checklist::where('user_id', '=', Auth::guard('api')->user()->id)->paginate(5);
        // return response()->json($checklists, 206);
        return new ChecklistCollection($checklists);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApiChecklist $request)
    {
        $user = Auth::guard('api')->user();
        $checklists = Checklist::where('user_id', '=', $user['id']);
        $data = $request->all();

        if ($data['user_id'] == $user['id'] && $checklists->count() < $user['allow_checklist']) {
            $checklist = Checklist::create($data);
            // return response()->json($checklist, 201);
            return new ChecklistResource($checklist);
        } else {
            return response()->json([
                'message' => 'Вы не можете совершить эти действия',
                'errors' => 'Доступ запрещен'
            ], 403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Checklist $checklist)
    {
        if ($checklist['user_id'] == Auth::guard('api')->user()->id) {
            // return response()->json($checklist, 200);
            return new ChecklistResource($checklist);
        } else {
            return response()->json([
                'message' => 'Вы не можете совершить эти действия',
                'errors' => 'Доступ запрещен'
            ], 403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Checklist $checklist)
    {
        $data = $request->all();

        if ($data['user_id'] == Auth::guard('api')->user()->id) {
            $checklist->update($request->all());
            return new ChecklistResource($checklist);
        } else {
            return response()->json([
                'message' => 'Вы не можете совершить эти действия',
                'errors' => 'Доступ запрещен'
            ], 403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Checklist $checklist)
    {
        $tasks = $checklist->tasks;

        if ($tasks->isEmpty()) {
            if ($checklist['user_id'] == Auth::guard('api')->user()->id) {
                $checklist->delete();
                return response()->json(null, 204);
            } else {
                return response()->json([
                'message' => 'Вы не можете совершить эти действия',
                'errors' => 'Доступ запрещен'
                ], 403);
            }
        } else {
            return response()->json([
                'message' => 'Вы не можете совершить эти действия. Сначала удалите задачи.',
                'errors' => 'Доступ запрещен'
            ], 403);
        }
    }
}
