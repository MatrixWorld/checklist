<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\Checklist;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ch_lists = Checklist::where('user_id', '=', Auth::user()->id)->paginate(5);
        return view('home', ['ch_lists' => $ch_lists]);
    }
}
