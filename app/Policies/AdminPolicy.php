<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the checklist.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function except_user(User $user)
    {
        if ($user['role'] == 'Admin' || $user['role'] == 'Moderator') {
            return true;
        }
    }

    /**
     * Determine whether the user can view the checklist.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function only_admin(User $user)
    {
        if ($user['role'] == 'Admin') {
            return true;
        }
    }

}
